// Code generated by go-swagger; DO NOT EDIT.

package op

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"errors"
	"net/url"
	golangswaggerpaths "path"

	"github.com/go-openapi/swag"
)

// CreateUnderPaintImageURL generates an URL for the create under paint image operation
type CreateUnderPaintImageURL struct {
	MaxLevel *int64
	Picture  *string
	Sp       *float64
	Sr       *float64

	_basePath string
	// avoid unkeyed usage
	_ struct{}
}

// WithBasePath sets the base path for this url builder, only required when it's different from the
// base path specified in the swagger spec.
// When the value of the base path is an empty string
func (o *CreateUnderPaintImageURL) WithBasePath(bp string) *CreateUnderPaintImageURL {
	o.SetBasePath(bp)
	return o
}

// SetBasePath sets the base path for this url builder, only required when it's different from the
// base path specified in the swagger spec.
// When the value of the base path is an empty string
func (o *CreateUnderPaintImageURL) SetBasePath(bp string) {
	o._basePath = bp
}

// Build a url path and query string
func (o *CreateUnderPaintImageURL) Build() (*url.URL, error) {
	var _result url.URL

	var _path = "/v1/under-paint"

	_basePath := o._basePath
	if _basePath == "" {
		_basePath = "/api"
	}
	_result.Path = golangswaggerpaths.Join(_basePath, _path)

	qs := make(url.Values)

	var maxLevelQ string
	if o.MaxLevel != nil {
		maxLevelQ = swag.FormatInt64(*o.MaxLevel)
	}
	if maxLevelQ != "" {
		qs.Set("maxLevel", maxLevelQ)
	}

	var pictureQ string
	if o.Picture != nil {
		pictureQ = *o.Picture
	}
	if pictureQ != "" {
		qs.Set("picture", pictureQ)
	}

	var spQ string
	if o.Sp != nil {
		spQ = swag.FormatFloat64(*o.Sp)
	}
	if spQ != "" {
		qs.Set("sp", spQ)
	}

	var srQ string
	if o.Sr != nil {
		srQ = swag.FormatFloat64(*o.Sr)
	}
	if srQ != "" {
		qs.Set("sr", srQ)
	}

	_result.RawQuery = qs.Encode()

	return &_result, nil
}

// Must is a helper function to panic when the url builder returns an error
func (o *CreateUnderPaintImageURL) Must(u *url.URL, err error) *url.URL {
	if err != nil {
		panic(err)
	}
	if u == nil {
		panic("url can't be nil")
	}
	return u
}

// String returns the string representation of the path with query string
func (o *CreateUnderPaintImageURL) String() string {
	return o.Must(o.Build()).String()
}

// BuildFull builds a full url with scheme, host, path and query string
func (o *CreateUnderPaintImageURL) BuildFull(scheme, host string) (*url.URL, error) {
	if scheme == "" {
		return nil, errors.New("scheme is required for a full url on CreateUnderPaintImageURL")
	}
	if host == "" {
		return nil, errors.New("host is required for a full url on CreateUnderPaintImageURL")
	}

	base, err := o.Build()
	if err != nil {
		return nil, err
	}

	base.Scheme = scheme
	base.Host = host
	return base, nil
}

// StringFull returns the string representation of a complete url
func (o *CreateUnderPaintImageURL) StringFull(scheme, host string) string {
	return o.Must(o.BuildFull(scheme, host)).String()
}
