// Code generated by go-swagger; DO NOT EDIT.

package op

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// CreateUnderPaintImageHandlerFunc turns a function with the right signature into a create under paint image handler
type CreateUnderPaintImageHandlerFunc func(CreateUnderPaintImageParams) CreateUnderPaintImageResponder

// Handle executing the request and returning a response
func (fn CreateUnderPaintImageHandlerFunc) Handle(params CreateUnderPaintImageParams) CreateUnderPaintImageResponder {
	return fn(params)
}

// CreateUnderPaintImageHandler interface for that can handle valid create under paint image params
type CreateUnderPaintImageHandler interface {
	Handle(CreateUnderPaintImageParams) CreateUnderPaintImageResponder
}

// NewCreateUnderPaintImage creates a new http.Handler for the create under paint image operation
func NewCreateUnderPaintImage(ctx *middleware.Context, handler CreateUnderPaintImageHandler) *CreateUnderPaintImage {
	return &CreateUnderPaintImage{Context: ctx, Handler: handler}
}

/* CreateUnderPaintImage swagger:route GET /v1/under-paint createUnderPaintImage

createUnderPaintImage

*/
type CreateUnderPaintImage struct {
	Context *middleware.Context
	Handler CreateUnderPaintImageHandler
}

func (o *CreateUnderPaintImage) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		*r = *rCtx
	}
	var Params = NewCreateUnderPaintImageParams()
	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request
	o.Context.Respond(rw, r, route.Produces, route, res)

}

// CreateUnderPaintImageOKBody create under paint image o k body
//
// swagger:model CreateUnderPaintImageOKBody
type CreateUnderPaintImageOKBody struct {

	// result
	// Required: true
	Result interface{} `json:"result"`
}

// UnmarshalJSON unmarshals this object while disallowing additional properties from JSON
func (o *CreateUnderPaintImageOKBody) UnmarshalJSON(data []byte) error {
	var props struct {

		// result
		// Required: true
		Result interface{} `json:"result"`
	}

	dec := json.NewDecoder(bytes.NewReader(data))
	dec.DisallowUnknownFields()
	if err := dec.Decode(&props); err != nil {
		return err
	}

	o.Result = props.Result
	return nil
}

// Validate validates this create under paint image o k body
func (o *CreateUnderPaintImageOKBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateResult(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *CreateUnderPaintImageOKBody) validateResult(formats strfmt.Registry) error {

	if o.Result == nil {
		return errors.Required("createUnderPaintImageOK"+"."+"result", "body", nil)
	}

	return nil
}

// ContextValidate validates this create under paint image o k body based on context it is used
func (o *CreateUnderPaintImageOKBody) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (o *CreateUnderPaintImageOKBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *CreateUnderPaintImageOKBody) UnmarshalBinary(b []byte) error {
	var res CreateUnderPaintImageOKBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}
