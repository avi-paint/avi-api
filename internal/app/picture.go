package app

import (
	"context"

	api "gitlab.com/avi-paint/proto/image-service-proto/go/v1/proc"
)

func (a *App) PyrMeanShiftFilter(ctx context.Context, in *api.PyrMeanShiftFilterRequest) (*api.DefaultReply, error) {
	req := api.PyrMeanShiftFilterRequest{
		Picture:  in.Picture,
	}
	return a.imageProcApi.PyrMeanShiftFilter(ctx, &req)
}

func (a *App) DisplayMainContours(ctx context.Context, in *api.DisplayContoursRequest) (*api.DefaultReply, error) {
	req := api.DisplayContoursRequest{
		Picture:  in.Picture,
	}
	return a.imageProcApi.DisplayMainContours(ctx, &req)
}
