package imageProcApi

import (
	"context"
	"log"
	"net"
	"net/http"
	"time"

	api "gitlab.com/avi-paint/proto/image-service-proto/go/v1/proc"
	"google.golang.org/grpc"

	"gitlab.com/avi-paint/avi-api/internal/cache"
	"gitlab.com/avi-paint/avi-api/internal/cache/memory"
)

const (
	cachedTime  = 5 * time.Minute
	clearedTime = 30 * time.Minute
	address     = ":10000"
)

type Api interface {
	PyrMeanShiftFilter(ctx context.Context, in *api.PyrMeanShiftFilterRequest) (*api.DefaultReply, error)
	DisplayMainContours(ctx context.Context, in *api.DisplayContoursRequest) (*api.DefaultReply, error)
}

type imageProcApi struct {
	client                  *http.Client
	storage                 cache.Storage
	processingServiceClient api.ImageProcessingServiceClient
}

func newImageProcessingClient() api.ImageProcessingServiceClient {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	return api.NewImageProcessingServiceClient(conn)
}

func New() Api {
	defaultTransport := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}).DialContext,
		MaxIdleConns:        20,
		MaxIdleConnsPerHost: 20,
		TLSHandshakeTimeout: 15 * time.Second,
	}

	client := &http.Client{
		Transport: defaultTransport,
		Timeout:   15 * time.Second,
	}

	return &imageProcApi{
		client:                  client,
		storage:                 memory.InitCash(cachedTime, clearedTime),
		processingServiceClient: newImageProcessingClient(),
	}
}
