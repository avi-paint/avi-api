package imageProcApi

import (
	"context"

	api "gitlab.com/avi-paint/proto/image-service-proto/go/v1/proc"
)

func (i *imageProcApi) PyrMeanShiftFilter(ctx context.Context, in *api.PyrMeanShiftFilterRequest) (*api.DefaultReply, error) {
	req := api.PyrMeanShiftFilterRequest{
		Picture:  in.Picture,
		Sp:       in.Sp,
		Sr:       in.Sr,
		MaxLevel: in.MaxLevel,
	}
	return i.processingServiceClient.PyrMeanShiftFilter(ctx, &req)
}

func (i *imageProcApi) DisplayMainContours(ctx context.Context, in *api.DisplayContoursRequest) (*api.DefaultReply, error) {
	req := api.DisplayContoursRequest{
		Picture: in.Picture,
	}
	return i.processingServiceClient.DisplayContoursDefault(ctx, &req)
}
