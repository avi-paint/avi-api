//go:generate gobin -m -run github.com/cheekybits/genny -in=$GOFILE -out=gen.$GOFILE gen "HealthCheck=PyrMeanShiftFilter"
//go:generate sed -i -e "\\,^//go:generate,d" gen.$GOFILE

package openapi

import (
	"net/http"

	"github.com/go-openapi/swag"

	"gitlab.com/avi-paint/avi-api/api/openapi/model"
	"gitlab.com/avi-paint/avi-api/api/openapi/restapi/op"
	"gitlab.com/avi-paint/avi-api/pkg/def"
)

func errHealthCheck(log Log, err error, code errCode) op.HealthCheckResponder {
	return op.NewHealthCheckDefault(code.status).WithPayload(&model.Error{
		Code:    swag.Int32(code.extra),
		Message: swag.String(logger(log, err, code)),
	})
}

func errCreateUnderPaintImage(log Log, err error, code errCode) op.CreateUnderPaintImageResponder {
	return op.NewCreateUnderPaintImageDefault(code.status).WithPayload(&model.Error{
		Code:    swag.Int32(code.extra),
		Message: swag.String(logger(log, err, code)),
	})
}

func errDisplayMainContours(log Log, err error, code errCode) op.DisplayMainContoursResponder {
	return op.NewDisplayMainContoursDefault(code.status).WithPayload(&model.Error{
		Code:    swag.Int32(code.extra),
		Message: swag.String(logger(log, err, code)),
	})
}

func logger(log Log, err error, code errCode) string {
	if code.status < http.StatusInternalServerError {
		log.Info("client error", def.LogHTTPStatus, code.status, "code", code.extra, "err", err)
	} else {
		log.PrintErr("server error", def.LogHTTPStatus, code.status, "code", code.extra, "err", err)
	}

	msg := err.Error()
	if code.status == http.StatusInternalServerError {
		msg = "internal error"
	}
	return msg
}
