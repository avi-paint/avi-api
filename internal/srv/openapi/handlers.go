package openapi

import (
	api "gitlab.com/avi-paint/proto/image-service-proto/go/v1/proc"

	"gitlab.com/avi-paint/avi-api/api/openapi/restapi/op"
)

func (srv *server) HealthCheck(params op.HealthCheckParams) op.HealthCheckResponder {
	ctx, log := fromRequest(params.HTTPRequest)
	status, err := srv.app.HealthCheck(ctx)
	switch {
	default:
		return errHealthCheck(log, err, codeInternal)
	case err == nil:
		return op.NewHealthCheckOK().WithPayload(status)
	}
}

func (srv *server) CreateUnderPaintImage(params op.CreateUnderPaintImageParams) op.CreateUnderPaintImageResponder {
	ctx, log := fromRequest(params.HTTPRequest)
	status, err := srv.app.PyrMeanShiftFilter(ctx, &api.PyrMeanShiftFilterRequest{
		Picture:  nil,
		Sp:       0,
		Sr:       0,
		MaxLevel: 0,
	})
	switch {
	default:
		return errCreateUnderPaintImage(log, err, codeInternal)
	case err == nil:
		return op.NewCreateUnderPaintImageOK().WithPayload(&op.CreateUnderPaintImageOKBody{Result: status})
	}
}

func (srv *server) DisplayMainContours(params op.DisplayMainContoursParams) op.DisplayMainContoursResponder {
	ctx, log := fromRequest(params.HTTPRequest)
	status, err := srv.app.DisplayMainContours(ctx, &api.DisplayContoursRequest{
		Picture: nil,
	})
	switch {
	default:
		return errDisplayMainContours(log, err, codeInternal)
	case err == nil:
		return op.NewDisplayMainContoursOK().WithPayload(&op.DisplayMainContoursOKBody{Result: status})
	}
}
